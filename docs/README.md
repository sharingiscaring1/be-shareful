﻿

# Patterns

## Creational

### Singleton

![singleton](https://i.postimg.cc/TYCmWD6S/singleton.png)

## Structural

### Decorator

![decorator](https://i.postimg.cc/CLQNK2fy/decorator.png)

### Proxy

![proxy](https://i.postimg.cc/433d1YQS/proxy.png)

### Facade

![facade](https://i.postimg.cc/DytjHHWn/facade.png)

## Behavioral

### Strategy
![strategy](https://i.postimg.cc/j2PYgffv/strategy.png)

### Chain of Responsibility

![cor](https://i.postimg.cc/xTsp5qQJ/cor.png)

### Command
![enter image description here](https://i.postimg.cc/mZzqG09t/command.png)
