﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Logging
{
    public class Logger : ILogger
    {
        protected ILogger nextLevelLogger;

        public virtual bool ShouldLog(ELoggingType loggingType) => loggingType == ELoggingType.Console;

        public Logger(ILogger nextLevelLogger = null)
        {
            this.nextLevelLogger = nextLevelLogger;
        }

        public virtual void Log(ELevel level, ELoggingType loggingType, object content)
        {
            if (ShouldLog(loggingType) && content is string message)
            {
                Console.WriteLine(PrepareFinalMessage(level, message));
            }
            else
            {
                nextLevelLogger?.Log(level, loggingType, content);
            }
        }

        private string PrepareFinalMessage(ELevel level, string message)
        {
            return $"[{level}] | {DateTime.Now} | {message}";
        }

    }
}
