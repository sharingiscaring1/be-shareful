﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Drawing.Imaging;
using System.IO;

namespace BeShareful.Utilities.Logging
{
    class GraphicsLogger : Logger, ILogger
    {
        public GraphicsLogger(ILogger nextLevelLogger = null) 
            : base(nextLevelLogger)
        {}

        public override bool ShouldLog(ELoggingType loggingType) => loggingType == ELoggingType.Graphics;

        public override void Log(ELevel level, ELoggingType loggingType, object content)
        {
            if(ShouldLog(loggingType) && content is Bitmap bitmap)
            {
                bitmap.Save(Path.Combine(Configuration.Instance.DefaultGraphicsLoggingPath, $"{DateTime.Now:ddMMyyyy-hhmmss}.png"));
            }
            else
            {
                nextLevelLogger?.Log(level, loggingType, content);
            }
        }
    }
}
