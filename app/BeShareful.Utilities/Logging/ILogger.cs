﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Logging
{
    public interface ILogger
    {
        void Log(ELevel level, ELoggingType loggingType, object content);
    }
}
