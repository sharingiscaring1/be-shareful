﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace BeShareful.Utilities.Logging
{
    class FileLogger : Logger, ILogger
    {
        public FileLogger(ILogger nextLevelLogger = null) 
            : base(nextLevelLogger)
        { }

        public override bool ShouldLog(ELoggingType loggingType) => loggingType == ELoggingType.PlainTextFile;

        public override async void Log(ELevel level, ELoggingType loggingType, object content)
        {
            if(ShouldLog(loggingType) && content is string message)
            {
                using (StreamWriter outputFile = File.AppendText(Configuration.Instance.DefaultLoggingPath))
                {
                    await outputFile.WriteAsync(PrepareFinalMessage(level, message));
                }
            }
            else
            {
                nextLevelLogger?.Log(level, loggingType, content);
            }
        }

        private string PrepareFinalMessage(ELevel level, string message)
        {
            return $"[{level}] | {DateTime.Now} | {message} {Environment.NewLine}";
        }
    }
}
