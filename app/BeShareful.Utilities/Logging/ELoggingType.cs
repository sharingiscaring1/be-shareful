﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Logging
{
    public enum ELoggingType
    {
        Console,
        PlainTextFile,
        Graphics
    }
}
