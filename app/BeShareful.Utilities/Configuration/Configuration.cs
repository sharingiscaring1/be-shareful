﻿using BeShareful.Utilities.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BeShareful.Utilities
{
    public class Configuration
    {
        public string DefaultLoggingPath { get; private set; }
        public string DefaultGraphicsLoggingPath { get; private set; }

        private static readonly object lockObject = new object();
        private static Configuration instance;
        public static Configuration Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new Configuration();
                        }
                    }
                }
                return instance;
            }
        }

        private readonly ILogger defaultLogger;

        private static readonly string ApplicationLoggingPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "BeShareful/Logging");
        private static readonly string LogFilePath = Path.Combine(ApplicationLoggingPath, "log.txt");
        private static readonly string LogScreenshotsPath = Path.Combine(ApplicationLoggingPath, "Screenshots/");

        private void EnsureLoggingDirectoriesExist()
        {
            if (!Directory.Exists(ApplicationLoggingPath))
            {
                Directory.CreateDirectory(ApplicationLoggingPath);
            }

            if (!Directory.Exists(LogScreenshotsPath))
            {
                Directory.CreateDirectory(LogScreenshotsPath);
            }
        }

        public void Log(ELevel level, ELoggingType loggingType, object content) => defaultLogger.Log(level, loggingType, content);

        private Configuration()
        {
            EnsureLoggingDirectoriesExist();
            
            DefaultLoggingPath = LogFilePath;
            DefaultGraphicsLoggingPath = LogScreenshotsPath;

            var graphicsLogger = new GraphicsLogger();
            var fileLogger = new FileLogger(graphicsLogger);
            var consoleLogger = new Logger(fileLogger);


            defaultLogger = consoleLogger;
        }
    }
}
