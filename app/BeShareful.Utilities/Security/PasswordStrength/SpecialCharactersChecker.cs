﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace BeShareful.Utilities.Security.PasswordStrength
{
    class SpecialCharactersChecker : StrengthChecker
    {
        private static readonly ISet<char> SpecialCharacters = new HashSet<char>()
        {
            '!','@','#','$','%','^','&','*','(',')','+','=','-','_','?','/','>','<'
        };

        private static readonly int Threshold = 2;

        public SpecialCharactersChecker(StrengthChecker checker = null) 
            : base(checker)
        {}

        protected override bool HasVerificationPassed(string password) => password.Count(c => SpecialCharacters.Contains(c)) >= Threshold;
    }
}
