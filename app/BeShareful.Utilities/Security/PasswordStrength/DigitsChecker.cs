﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeShareful.Utilities.Security.PasswordStrength
{
    class DigitsChecker : StrengthChecker
    {
        private static readonly int Threshold = 1;
        public DigitsChecker(StrengthChecker checker = null) 
            : base(checker)
        {}

        protected override bool HasVerificationPassed(string password) => password.Count(c => char.IsDigit(c)) >= Threshold;
    }
}
