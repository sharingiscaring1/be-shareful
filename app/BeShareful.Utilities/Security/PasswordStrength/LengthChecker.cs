﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security.PasswordStrength
{
    class LengthChecker : StrengthChecker
    {
        private static readonly int MinimumNumberOfCharacters = 5;

        public LengthChecker(StrengthChecker checker = null) 
            : base(checker)
        {}

        protected override bool HasVerificationPassed(string password) => password.Length >= MinimumNumberOfCharacters;
    }
}
