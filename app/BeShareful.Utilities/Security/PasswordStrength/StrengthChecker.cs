﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security.PasswordStrength
{
    class StrengthChecker
    {
        protected StrengthChecker nextLevelChecker;

        public StrengthChecker(StrengthChecker checker = null)
        {
            nextLevelChecker = checker;
        }

        public bool IsSecure(string password)
        {
            bool hasVerificationPassed = HasVerificationPassed(password);
            if (hasVerificationPassed)
            {
                if(nextLevelChecker != null)
                {
                    return nextLevelChecker.IsSecure(password);
                }
                return hasVerificationPassed;
            }
            return false;
        }

        protected virtual bool HasVerificationPassed(string password) => true;
    }
}
