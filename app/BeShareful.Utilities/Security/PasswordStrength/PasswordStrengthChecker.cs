﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security.PasswordStrength
{
    public class PasswordStrengthChecker
    {
        private StrengthChecker strengthChecker;
        public PasswordStrengthChecker()
        {
            strengthChecker = new StrengthChecker(new LengthChecker(new DigitsChecker(new SpecialCharactersChecker())));
        }

        public bool IsSecure(string password) => strengthChecker.IsSecure(password);
    }
}
