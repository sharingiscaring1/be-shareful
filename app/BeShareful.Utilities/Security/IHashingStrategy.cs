﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security
{
    public interface IHashingStrategy
    {
        string GetHash(string value);
        bool Check(string value, string hash);
    }
}
