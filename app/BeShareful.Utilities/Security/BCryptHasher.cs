﻿using BCrypt.Net;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security
{
    public class BCryptHasher : IHashingStrategy
    {
        private SaltRevision saltRevision;

        public BCryptHasher(SaltRevision saltRevision = SaltRevision.Revision2A)
        {
            this.saltRevision = saltRevision;
        }

        public bool Check(string value, string hash) => BCrypt.Net.BCrypt.Verify(value, hash);

        public string GetHash(string value)
        {
            return BCrypt.Net.BCrypt.HashPassword(value, saltRevision);
        }
    }
}
