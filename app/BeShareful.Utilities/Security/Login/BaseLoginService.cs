﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security.Login
{
    public class BaseLoginService : ILoginService
    {
        public IHashingStrategy HashingStrategy { get; }
        public BaseLoginService(IHashingStrategy hashingStrategy)
        {
            HashingStrategy = hashingStrategy;
        }

        public bool DoLogin(string password, string hash) => HashingStrategy.Check(password, hash);
    }
}
