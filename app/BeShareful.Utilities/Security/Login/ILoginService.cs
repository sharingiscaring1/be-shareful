﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Utilities.Security.Login
{
    public interface ILoginService
    {
        IHashingStrategy HashingStrategy { get; }

        bool DoLogin(string password, string hash);
    }
}
