﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BeShareful.Utilities.Security
{
    public class SHA256Hasher : IHashingStrategy
    {
        public bool Check(string value, string hash)  => hash.Equals(GetHash(value));

        public string GetHash(string value)
        {
            using (SHA256 hash = SHA256.Create())
            {
                var bytes = hash.ComputeHash(Encoding.UTF8.GetBytes(value));
                StringBuilder builder = new StringBuilder();
                for (int index = 0; index < bytes.Length; index++)
                {
                    builder.Append(bytes[index].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
