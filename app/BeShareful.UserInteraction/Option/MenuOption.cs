﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.UserInteraction.Option
{
    class MenuOption
    {
        public int Index { get; }
        public string Title { get; }
        public string Content { get; }
        public Func<string, bool> InputValidator { get; }

        public MenuOption(int index, string title, string content, Func<string, bool> inputValidator)
        {
            Index = index;
            Title = title;
            Content = content;
            InputValidator = inputValidator;
        }

        public bool IsValid(string input) => InputValidator(input);
    }
}
