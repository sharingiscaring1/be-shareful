﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.UserInteraction.State
{
    interface IMenuState
    {
        void Show();
    }
}
