﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Join;
using BeShareful.Entities.Entity.Metadata;
using Microsoft.EntityFrameworkCore;

namespace BeShareful.Storage.Context
{
    public class BeSharefulContext : DbContext
    {
        public BeSharefulContext(DbContextOptions<BeSharefulContext> options)
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Trade> Trades { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<OfferSubscriber> OfferSubscribers { get; set; }
        public DbSet<UserMetadata> UserMetadatas { get; set; }
        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();

            modelBuilder.Entity<User>()
                .HasMany(u => u.PostedOffers)
                .WithOne(o => o.Owner);

            modelBuilder.Entity<User>()
                .HasMany(u => u.MediatedOffers)
                .WithOne(o => o.Mediator);

            modelBuilder.Entity<Offer>()
                .HasOne(o => o.Owner)
                .WithMany(u => u.PostedOffers)
                .HasForeignKey(o => o.OwnerId);

            modelBuilder.Entity<OfferSubscriber>()
                .HasKey(os => new { os.OfferId, os.SubscriberId });

            modelBuilder.Entity<OfferSubscriber>()
                .HasOne(o => o.Offer)
                .WithMany(s => s.Subscribers)
                .HasForeignKey(os => os.OfferId);

            modelBuilder.Entity<OfferSubscriber>()
               .HasOne(s => s.Subscriber)
               .WithMany(o => o.SubscribedOffers)
               .HasForeignKey(os => os.SubscriberId);

            //Messages
            modelBuilder.Entity<User>()
                .HasMany(u => u.ReceivedMessages)
                .WithOne(m => m.Receiver);
        }
    }
}
