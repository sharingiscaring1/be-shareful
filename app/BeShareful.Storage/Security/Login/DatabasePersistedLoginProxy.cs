﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeShareful.Entities.Entity;
using BeShareful.Entities.Security;
using BeShareful.Storage.Context;
using BeShareful.Storage.Initializer;
using BeShareful.Utilities;
using BeShareful.Utilities.Security;
using BeShareful.Utilities.Security.Login;
using Microsoft.EntityFrameworkCore;

namespace BeShareful.Storage.Security.Login
{
    public class DatabasePersistedLoginProxy
    {
        private ILoginService loginService;
        private BeSharefulContext databaseContext;

        private static readonly IReadOnlyDictionary<Hashing.Type, IHashingStrategy> HashingTypeToStrategy = new Dictionary<Hashing.Type, IHashingStrategy>()
        {
            {Hashing.Type.BCrypt, new BCryptHasher() },
            {Hashing.Type.SHA256, new SHA256Hasher() },
        };

        public DatabasePersistedLoginProxy()
        {
            databaseContext = BeSharefulDatabaseInitializer.GetDefaultContext();
        }

        public async Task<(bool, string, User)> DoLogin(string username, string password)
        {
            var user = await databaseContext.Users.FirstOrDefaultAsync(u => u.Username == username);
            if(user == null)
            {
                return (false, "The user does not exist.", user);
            }
            
            if(HashingTypeToStrategy.ContainsKey(user.HashingType))
            {
                loginService = new BaseLoginService(HashingTypeToStrategy[user.HashingType]);
                var success = loginService.DoLogin(password, user.PasswordHash);
                return success ? (success, "Succesful login.", user) : (success, "The password did not match.", user);
            }

            Configuration.Instance.Log(Utilities.Logging.ELevel.Warning, Utilities.Logging.ELoggingType.PlainTextFile , $"There was an attempt to login by using a different hashing type than the ones known by the application.");
            return (false, "Internal error. Please contact the application owner.", null);
        }
    }
}
