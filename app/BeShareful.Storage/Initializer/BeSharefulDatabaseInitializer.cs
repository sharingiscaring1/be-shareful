﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.Entities.Factory;
using BeShareful.Storage.Context;
using BeShareful.Utilities.Security;
using Microsoft.EntityFrameworkCore;
using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace BeShareful.Storage.Initializer
{
    public static class BeSharefulDatabaseInitializer
    {
        public static readonly string DefaultConnectionName = "BeSharefulDB";

        private static readonly ConnectionStringSettingsCollection ConnectionStrings;

        public static string DefaultConnectionString => ConnectionStrings[DefaultConnectionName].ConnectionString;
            
        static BeSharefulDatabaseInitializer()
        {
            ConnectionStrings = ConfigurationManager.ConnectionStrings;
        }

        public static BeSharefulContext GetContext(string connectionString)
        {
            var options = new DbContextOptionsBuilder<BeSharefulContext>();
            options.UseMySql(connectionString);
            return new BeSharefulContext(options.Options);
        }

        public static BeSharefulContext GetDefaultContext() => GetContext(DefaultConnectionString);

        public static async Task SeedDefaultValuesAsync(this BeSharefulContext context)
        {
            if(await context.Users.AnyAsync(u => u.Type == EUserType.Admin))
            {
                return;
            }

            var userFactory = new UserFactory(new BCryptHasher());

            context.Users.Add(userFactory.Create("bruce_wayne", ConfigurationManager.AppSettings["bw_pass"], "Bruce", "Wayne", EUserGender.Male, "073286534", "Batcave", EUserType.Admin));
            context.Users.Add(userFactory.Create("ethan_hunt", ConfigurationManager.AppSettings["et_pass"], "Ethan", "Hunt", EUserGender.Male, "0757873278", "Kansas City BBQ, 600 West Harbor Drive", EUserType.Admin));

            await context.SaveChangesAsync();
        }

        public static async Task EnsureDatabaseIsProperlyInitializedAsync()
        {
            var context = GetContext(DefaultConnectionString);

            await context.Database.EnsureCreatedAsync();
            await context.SeedDefaultValuesAsync();
        }

        public static async Task EnsureDatabaseIsProperlyInitializedAsync(string connectionString)
        {
            var context = GetContext(connectionString);

            await context.Database.EnsureCreatedAsync();
            //await context.SeedDefaultValuesAsync();
        }
    }
}
