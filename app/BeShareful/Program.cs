﻿using BeShareful.Entities.Factory;
using BeShareful.Utilities;
using BeShareful.Utilities.Logging;
using BeShareful.Entities.Entity.Enum.UserMetadata;
using BeShareful.Utilities.Security;
using BeShareful.Entities.Entity.Metadata.Decorator;
using BeShareful.Entities.Entity.Metadata;
using BeShareful.Entities.Entity;
using BeShareful.Storage.Initializer;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BeShareful
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Configuration.Instance.Log(ELevel.Error, ELoggingType.PlainTextFile, "Hello");
            Configuration.Instance.Log(ELevel.Error, ELoggingType.Console, "Hello");
            Configuration.Instance.Log(ELevel.Info, ELoggingType.Console, "World");
            Configuration.Instance.Log(ELevel.Warning, ELoggingType.Console, "Hello Home!");


            //var factory2 = new UserFactory(new SHA256Hasher());
            //var admin = factory2.Create("x", "salut", "y", "z", Entities.Entity.Enum.EUserGender.Female, "0732206052", "Tohanu Nou", Entities.Entity.Enum.EUserType.Admin);
            //var regular = factory2.Create("admin", "salut", "y", "z", Entities.Entity.Enum.EUserGender.Female, "073106052", "Tohanu Nou", Entities.Entity.Enum.EUserType.Regular);

            //var sub1 = factory2.Create("subscriber1", "salut", "y", "z", Entities.Entity.Enum.EUserGender.Female, "0731306023", "Tohanu Nou", Entities.Entity.Enum.EUserType.Regular);
            //var sub2 = factory2.Create("subscriber2", "salut", "y", "z", Entities.Entity.Enum.EUserGender.Female, "0731301053", "Tohanu Nou", Entities.Entity.Enum.EUserType.Regular);

            //var newUserMetadata = new ProfessionalStatus(EProfessionalStatus.Employed, new NetIncome(5000, new DomainOfWork(EDomainOfWork.IT, new BasicUserMetadata())));
            //double score = newUserMetadata.ComputeScore();
            //var metadatas = newUserMetadata.ProvidedMetadatas;

            //Product product = new Product("xyz", 120.0, Entities.Entity.Enum.EProductState.New, "abc", "def", Entities.Entity.Enum.ECategoryType.Books);


            //var context = BeSharefulDatabaseInitializer.GetContext("connectionString");
            //await BeSharefulDatabaseInitializer.EnsureDatabaseIsProperlyInitializedAsync("connectionString");
            //admin = context.Users.Add(admin).Entity;
            //regular = context.Users.Add(regular).Entity;
            //sub1 = context.Users.Add(sub1).Entity;
            //sub2 = context.Users.Add(sub2).Entity;

            //Offer offer = new Offer(regular, product, 1, "brasov")
            //{
            //    Mediator = admin
            //};

            //offer.Subscribers.Add(new Entities.Entity.Join.OfferSubscriber(offer, sub1));
            //offer.Subscribers.Add(new Entities.Entity.Join.OfferSubscriber(offer, sub2));

            //offer = context.Offers.Add(offer).Entity;

            //Message message1 = new Message("Salutare", DateTime.Now, regular);
            //context.Messages.Add(message1);

            //await context.SaveChangesAsync();


            //var messages =  context.Messages.Where(u => u.Receiver.Username=="admin").ToList();

        }
    }
}
