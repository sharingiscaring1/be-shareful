﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.GUI.Result
{
    public class ExecutionResult
    {
        public bool Success { get; set; }
        public string Message { get; set; } = string.Empty;

        public ExecutionResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}
