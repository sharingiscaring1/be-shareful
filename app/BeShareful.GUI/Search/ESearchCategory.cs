﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.GUI.Search
{
    public enum ESearchCategory
    {
        Name,
        State,
        Manufacturer,
        Status,
        Location
    }
}
