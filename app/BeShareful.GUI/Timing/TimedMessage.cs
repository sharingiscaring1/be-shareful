﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Timers;

namespace BeShareful.GUI.Timing
{
    public class TimedMessage : INotifyPropertyChanged
    {
        private readonly Timer messageTimer;

        private string _message;
        public string Value
        {
            get => _message;
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    messageTimer.Start();
                }
                _message = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public TimedMessage(int timeoutMiliseconds = 5000)
        {
            Value = string.Empty;
            messageTimer = new Timer();
            messageTimer.Interval = timeoutMiliseconds;
            messageTimer.Elapsed += MessageTimer_Elapsed;
        }

        private void MessageTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Value = string.Empty;
            messageTimer.Stop();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
