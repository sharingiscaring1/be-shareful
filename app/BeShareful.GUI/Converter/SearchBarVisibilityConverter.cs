﻿using BeShareful.Entities.Entity.Enum;
using BeShareful.GUI.Search;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace BeShareful.GUI.Converter
{
    class SearchBarVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is ESearchCategory searchCategory)
            {
                return (searchCategory != ESearchCategory.Status && searchCategory != ESearchCategory.State) ? Visibility.Visible : Visibility.Hidden;
            }
            else
            {
                return Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
