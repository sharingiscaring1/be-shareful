﻿using BeShareful.Storage.Initializer;
using System.Windows;

namespace BeShareful.GUI
{
    public partial class App : Application
    {
        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            await BeSharefulDatabaseInitializer.EnsureDatabaseIsProperlyInitializedAsync();
        }
    }
}
