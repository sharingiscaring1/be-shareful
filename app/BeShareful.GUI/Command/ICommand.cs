﻿using BeShareful.GUI.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeShareful.GUI.Command
{
    public interface ICommand
    {
        Task<ExecutionResult> ExecuteAsync();

        ExecutionResult Execute();

        Task<ExecutionResult> UndoAsync();

        ExecutionResult Undo();
    }
}
