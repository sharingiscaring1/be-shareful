﻿using BeShareful.Entities.Entity;
using BeShareful.GUI.Result;
using BeShareful.GUI.ViewModel.CustomView;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeShareful.GUI.Command
{
    class UserDashboardUICommand : ICommand
    {
        private ManageOffersViewModel viewModel;
        private Offer offer;
        private ECommandType commandType;

        public UserDashboardUICommand(ECommandType commandType, ManageOffersViewModel viewModel, Offer offer)
        {
            this.commandType = commandType;
            this.viewModel = viewModel;
            this.offer = offer;
        }

        public ExecutionResult Execute() => viewModel.Execute(commandType, offer);

        public async Task<ExecutionResult> ExecuteAsync() => await viewModel.ExecuteAsync(commandType, offer);

        public ExecutionResult Undo() => viewModel.Undo(commandType, offer);

        public async Task<ExecutionResult> UndoAsync() => await viewModel.UndoAsync(commandType, offer);
    }
}
