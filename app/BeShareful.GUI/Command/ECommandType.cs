﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.GUI.Command
{
    public enum ECommandType
    {
        Add,
        Delete,
        Edit
    }
}
