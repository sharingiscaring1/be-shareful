﻿using BeShareful.Entities.Entity;
using BeShareful.GUI.Result;
using BeShareful.GUI.ViewModel.CustomView;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeShareful.GUI.Command
{
    public class UserDashboardUIInvoker
    {
        private Stack<ICommand> commands;
        private readonly ManageOffersViewModel viewModel;

        public UserDashboardUIInvoker(ManageOffersViewModel viewModel)
        {
            commands = new Stack<ICommand>();
            this.viewModel = viewModel;
        }

        public async Task<ExecutionResult> ExecuteAsync(ECommandType commandType, Offer offer)
        {
            var command = new UserDashboardUICommand(commandType, viewModel, offer);
            var executionResult = await command.ExecuteAsync();
            commands.Push(command);
            return executionResult;
        }

        public async Task<ExecutionResult> Undo()
        {
            if(commands.Count > 0)
            {
                var executionResult = await commands.Peek().UndoAsync();
                commands.Pop();
                return executionResult;
            }
            else
            {
                return new ExecutionResult(false, "Can't undo.");
            }
        }
    }
}
