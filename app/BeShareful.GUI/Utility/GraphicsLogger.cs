﻿using BeShareful.Utilities.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows;

namespace BeShareful.GUI.Utility
{
    class GraphicsLogger
    {
        private static readonly Rect primaryScreen = SystemParameters.WorkArea;
        private static readonly double screenLeft = primaryScreen.Left;
        private static readonly double screenTop = primaryScreen.Top;
        private static readonly double screenWidth = primaryScreen.Width;
        private static readonly double screenHeight = primaryScreen.Height;

        public static void Log(ELevel level)
        {
            using (Bitmap bitmap = new Bitmap((int)screenWidth, (int)screenHeight))
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    graphics.CopyFromScreen((int)screenLeft, (int)screenTop, 0, 0, bitmap.Size);
                    Utilities.Configuration.Instance.Log(level, ELoggingType.Graphics, bitmap);
                }
            }
        }
    }
}
