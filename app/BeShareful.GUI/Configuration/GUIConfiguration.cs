﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;

namespace BeShareful.GUI.Configuration
{
    public class GUIConfiguration : INotifyPropertyChanged
    {
        private PaletteHelper paletteHelper;

        private ITheme CurrentTheme => paletteHelper.GetTheme();

        public IBaseTheme CurrentBaseTheme { get; private set; }
        public SolidColorBrush LightThemeTitleBarBrush { get; }
        public SolidColorBrush DarkThemeTitleBarBrush { get; }

        private Brush _currentThemeBrush;
        public Brush CurrentThemeBrush
        {
            get => _currentThemeBrush;
            set
            {
                _currentThemeBrush = value;
                OnPropertyChanged(nameof(CurrentThemeBrush));
            }
        }

        private static readonly object lockObject = new object();
        private static GUIConfiguration _instance;
        public static GUIConfiguration Instance
        {
            get
            {
                if(_instance == null)
                {
                    lock(lockObject)
                    {
                        if(_instance == null)
                        {
                            _instance = new GUIConfiguration();
                        }
                    }
                }
                return _instance;
            }
        }

        private GUIConfiguration()
        {
            LightThemeTitleBarBrush = Application.Current.Resources[nameof(LightThemeTitleBarBrush)] as SolidColorBrush;
            DarkThemeTitleBarBrush = Application.Current.Resources[nameof(DarkThemeTitleBarBrush)] as SolidColorBrush;
            CurrentThemeBrush = LightThemeTitleBarBrush;
            CurrentBaseTheme = Theme.Light;
            paletteHelper = new PaletteHelper();
        }

        public void SetBaseTheme(IBaseTheme newTheme)
        {
            CurrentBaseTheme = newTheme;
            CurrentTheme.SetBaseTheme(newTheme);
            paletteHelper.SetTheme(CurrentTheme);
        }


        public void SetPrimaryColor(Color color)
        {
            CurrentTheme.SetPrimaryColor(color);
            paletteHelper.SetTheme(CurrentTheme);
        }

        public void SetSecondaryColor(Color color)
        {
            CurrentTheme.SetSecondaryColor(color);
            paletteHelper.SetTheme(CurrentTheme);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
