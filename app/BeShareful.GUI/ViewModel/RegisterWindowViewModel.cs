﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.Storage.Context;
using BeShareful.Storage.Initializer;
using BeShareful.Utilities.Security.PasswordStrength;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Initializer = BeShareful.Storage.Initializer.BeSharefulDatabaseInitializer;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using BeShareful.Entities.Factory;
using BeShareful.Utilities.Security;
using BeShareful.GUI.Timing;

namespace BeShareful.GUI.ViewModel
{
    class RegisterWindowViewModel : INotifyPropertyChanged
    {
        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                _username = value;
                if (databaseContext.Users.FirstOrDefault(u => u.Username == value) != null)
                {
                    ErrorMessage.Value = "The username is already used.";
                }
                else
                {
                    ErrorMessage.Value = string.Empty;
                }
            }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EUserGender UserGender { get; set; }
        public string PhoneNumber { get; set; }
        public string Location { get; set; }
        public EUserType UserType { get; set; }



        private TimedMessage _message;
        public TimedMessage ErrorMessage
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }


        private static readonly string DefaultPasswordErrorMessage = "Your password is not secure enough.";

        private readonly BeSharefulContext databaseContext;
        private readonly PasswordStrengthChecker strengthChecker;

        private readonly IReadOnlyDictionary<string, UserFactory> HashingAlgorithmToFactory = new Dictionary<string, UserFactory>()
        {
            { nameof(SHA256Hasher), new UserFactory(new SHA256Hasher()) },
            { nameof(BCryptHasher), new UserFactory(new BCryptHasher()) },
        };

        public RegisterWindowViewModel()
        {
            ErrorMessage = new TimedMessage(2000);
            strengthChecker = new PasswordStrengthChecker();
            databaseContext = Initializer.GetDefaultContext();
        }

        private bool AreFieldsCompleted()
        {
            return !string.IsNullOrEmpty(Username)
                && !string.IsNullOrEmpty(FirstName)
                && !string.IsNullOrEmpty(LastName)
                && !string.IsNullOrEmpty(PhoneNumber)
                && !string.IsNullOrEmpty(Location);
        }

        public async Task<(bool, User)> RegisterAsync(string password)
        {
            if(AreFieldsCompleted())
            {
                if(strengthChecker.IsSecure(password))
                {
                    UserFactory factory = null;
                    if (UserType == EUserType.Admin)
                    {
                        factory = HashingAlgorithmToFactory[nameof(BCryptHasher)];
                    }
                    else
                    {
                        factory = HashingAlgorithmToFactory[nameof(SHA256Hasher)];
                    }

                    var user = factory.Create(Username, password, FirstName, LastName, UserGender, PhoneNumber, Location, UserType);
                    databaseContext.Users.Add(user);
                    try
                    {
                        await databaseContext.SaveChangesAsync();
                        return (true, user);
                    }
                    catch (DbUpdateException e)
                    {
                        ErrorMessage.Value = e.InnerException.Message;
                        databaseContext.Users.Remove(user);
                        return (false, null);
                    }
                }
                else
                {
                    ErrorMessage.Value = DefaultPasswordErrorMessage;
                    return (false, null);
                }
            }
            else
            {
                ErrorMessage.Value = "Please complete all the fields.";
                return (false, null);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
