﻿using BeShareful.Entities.Entity;
using BeShareful.Storage.Security.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BeShareful.GUI.ViewModel
{
    class MainWindowViewModel
    {
        private readonly DatabasePersistedLoginProxy loginProxy;

        public string Username { get; set; }

        public MainWindowViewModel()
        {
            loginProxy = new DatabasePersistedLoginProxy();
        }

        public async Task<(bool, string, User)> LoginAsync(string password)
        {
            var (success, message, user) = await loginProxy.DoLogin(Username, password);
            return (success, message, user);
        }

    }
}
