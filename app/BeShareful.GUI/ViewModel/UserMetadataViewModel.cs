﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum.UserMetadata;
using BeShareful.Entities.Entity.Metadata;
using BeShareful.Entities.Entity.Metadata.Decorator;
using Initializer = BeShareful.Storage.Initializer.BeSharefulDatabaseInitializer;
using BeShareful.Storage.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeShareful.Utilities.Logging;

namespace BeShareful.GUI.ViewModel
{
    class UserMetadataViewModel : INotifyPropertyChanged
    {
       
        public bool HasEnteredProfessionalStatus { get; set; }
        public EProfessionalStatus ProfessionalStatus { get; set; }
        public bool HasEnteredDomainOfWork { get; set; }
        public EDomainOfWork DomainOfWork { get; set; }
        public bool HasEnteredNetIncome { get; set; }
        public double NetIncome { get; set; }

        private User currentUser;

        private readonly IDictionary<(bool, bool, bool), Func<IUserMetadata>> ChoicesToMetadata = new Dictionary<(bool, bool, bool), Func<IUserMetadata>>();
        public UserMetadataViewModel(User user)
        {
            currentUser = user;
            PopulateChoicesDictionary();
            databaseContext = Initializer.GetDefaultContext();
        }

        private void PopulateChoicesDictionary()
        {
            ChoicesToMetadata.Add((false, false, false), () => new BasicUserMetadata());
            ChoicesToMetadata.Add((false, false, true),  () => new NetIncome(NetIncome, new BasicUserMetadata()));
            ChoicesToMetadata.Add((false, true, false),  () => new DomainOfWork(DomainOfWork, new BasicUserMetadata()));
            ChoicesToMetadata.Add((true, false, false),  () => new ProfessionalStatus(ProfessionalStatus, new BasicUserMetadata()));
            ChoicesToMetadata.Add((true, false, true),   () => new ProfessionalStatus(ProfessionalStatus, new NetIncome(NetIncome, new BasicUserMetadata())));
            ChoicesToMetadata.Add((true, true, false),   () => new ProfessionalStatus(ProfessionalStatus, new DomainOfWork(DomainOfWork, new BasicUserMetadata())));
            ChoicesToMetadata.Add((true, true, true),    () => new ProfessionalStatus(ProfessionalStatus, new DomainOfWork(DomainOfWork, new NetIncome(NetIncome, new BasicUserMetadata()))));
        }

        public UserMetadata BuildMetadata()
        {
            var decoratedMetadata = ChoicesToMetadata[(HasEnteredProfessionalStatus, HasEnteredDomainOfWork, HasEnteredNetIncome)]();
            var metadata = new UserMetadata(decoratedMetadata)
            {
                UserMetadataId = currentUser.UserId
            };
            return metadata;
        }

        private readonly BeSharefulContext databaseContext;

        public async Task<(bool, UserMetadata)> AddMetadataAsync()
        {
            var userMetadata = BuildMetadata();
            databaseContext.UserMetadatas.Add(userMetadata);
            databaseContext.Users.Find(currentUser.UserId).UserMetadataId = userMetadata.UserMetadataId;
            try
            {
                await databaseContext.SaveChangesAsync();
                BeShareful.Utilities.Configuration.Instance.Log(ELevel.Info, ELoggingType.Console, $"The user with username [{currentUser.Username}] added metadata.");
                return (true, userMetadata);
            }
            catch (DbUpdateException e)
            {
                databaseContext.UserMetadatas.Remove(userMetadata);
                BeShareful.Utilities.Configuration.Instance.Log(ELevel.Error, ELoggingType.PlainTextFile, $"Adding metadata for user [{currentUser.Username}] failed :"+ e);
                return (false, null);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
