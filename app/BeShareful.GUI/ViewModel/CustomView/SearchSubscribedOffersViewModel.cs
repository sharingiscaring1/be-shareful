﻿using BeShareful.Entities.Entity;
using BeShareful.Storage.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeShareful.GUI.ViewModel.CustomView
{
    class SearchSubscribedOffersViewModel: INotifyPropertyChanged
    {
        public SearchOffersViewModel _searchOffersViewModel;
        public SearchOffersViewModel SearchOffersViewModel
        {
            get => _searchOffersViewModel;
            set
            {
                _searchOffersViewModel = value;
                OnPropertyChanged(nameof(SearchOffersViewModel));
            }
        }

        public SearchSubscribedOffersViewModel(User user, BeSharefulContext context, bool filterValue)
        {
            SearchOffersViewModel = new SearchOffersViewModel(context, user);
            SearchOffersViewModel.FilterBySubscribed = filterValue;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
