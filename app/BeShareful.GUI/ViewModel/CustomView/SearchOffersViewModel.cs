﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.Entities.Entity.Join;
using BeShareful.GUI.Result;
using BeShareful.GUI.Search;
using BeShareful.Storage.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Initializer = BeShareful.Storage.Initializer.BeSharefulDatabaseInitializer;

namespace BeShareful.GUI.ViewModel.CustomView
{
    public class SearchOffersViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Offer> _offers;
        public ObservableCollection<Offer> Offers
        {
            get => _offers;
            set
            {
                _offers = value;
                OnPropertyChanged(nameof(Offers));
            }
        }

        public bool FilterBySubscribed { get; set; } = false;

        private readonly User currentUser;

        private readonly BeSharefulContext databaseContext;

        private readonly IDictionary<ESearchCategory, Func<object, BeSharefulContext, Task<List<Offer>>>> SearchCategoryToResults = new Dictionary<ESearchCategory, Func<object, BeSharefulContext, Task<List<Offer>>>>();

        public SearchOffersViewModel(BeSharefulContext context, User user)
        {
            currentUser = user;
            databaseContext = context;
            PopulateCategoryToResultsDictionary();
        }

        private void PopulateCategoryToResultsDictionary()
        {
            SearchCategoryToResults.Add(ESearchCategory.Manufacturer, async (value, context) =>
            {
                var stringValue = value as string;
                return await ComposeDefaultSearchQuery(context)
                             .Where(o => o.OfferedProduct.Manufacturer.Contains(stringValue))
                             .ToListAsync();
            });

            SearchCategoryToResults.Add(ESearchCategory.Location, async (value, context) =>
            {
                var stringValue = value as string;
                return await ComposeDefaultSearchQuery(context)
                             .Where(o => o.Location.Contains(stringValue))
                             .ToListAsync();
            });

            SearchCategoryToResults.Add(ESearchCategory.Name, async (value, context) =>
            {
                var stringValue = value as string;
                return await ComposeDefaultSearchQuery(context)
                             .Where(o => o.OfferedProduct.Name.Contains(stringValue))
                             .ToListAsync();
            });

            SearchCategoryToResults.Add(ESearchCategory.State, async (value, context) =>
            {
                var productState = (EProductState)value;
                return await ComposeDefaultSearchQuery(context)
                             .Where(o => o.OfferedProduct.State == productState)
                             .ToListAsync();
            });

            SearchCategoryToResults.Add(ESearchCategory.Status, async (value, context) =>
            {
                var offerStatus = (EOfferStatus)value;
                return await ComposeDefaultSearchQuery(context)
                             .Where(o => o.Status == offerStatus)
                             .ToListAsync();
            });
        }

        public async Task<ExecutionResult> SubscribeAsync(Offer offer)
        {
            var offerSubscriber = new Entities.Entity.Join.OfferSubscriber(offer.OfferId, currentUser.UserId);
            if (offer.Subscribers.Contains(offerSubscriber, new OfferSubscriberEqualityComparer()))
            {
                return new ExecutionResult(false, "You have already subscribed to this offer.");
            }
            
            offer.Subscribers.Add(offerSubscriber);
            databaseContext.Offers.Update(offer);

            try
            {
                await databaseContext.SaveChangesAsync();
                Offers.Remove(offer);
                return new ExecutionResult(true, "You've subscribed successfully to this offer.");
            }
            catch (DbUpdateException e)
            {
                return new ExecutionResult(false, e.InnerException.Message);
            }
        }

        public async Task<ExecutionResult> UnsubscribeAsync(Offer offer)
        {
            var offerSubscriber = new Entities.Entity.Join.OfferSubscriber(offer.OfferId, currentUser.UserId);
            if (!offer.Subscribers.Contains(offerSubscriber, new OfferSubscriberEqualityComparer()))
            {
                return new ExecutionResult(false, "You are not subscribed to this offer.");
            }
            offer.Subscribers.Remove(offer.Subscribers.Single(subscriber => subscriber.SubscriberId == offerSubscriber.SubscriberId));
            databaseContext.Offers.Update(offer);
            
            try
            {
                await databaseContext.SaveChangesAsync();
                Offers.Remove(offer);
                return new ExecutionResult(true, "You've successfully unsubscribed from this offer.");
            }
            catch (DbUpdateException e)
            {
                return new ExecutionResult(false, e.InnerException.Message);
            }
        }
    

        private IQueryable<Offer> ComposeDefaultSearchQuery(BeSharefulContext context)
        {
            return context.Offers
                .Include(o => o.OfferedProduct)
                .Include(o => o.Owner)
                .Include(o => o.Subscribers)
                .Where(o => o.Owner.UserId != currentUser.UserId);
        }

        public async Task LoadAllOffersAsync()
        {
            Offers = new ObservableCollection<Offer>( await databaseContext.Offers
                            .Include(o => o.OfferedProduct)
                            .Include(o => o.Owner)
                            .Include(o => o.Subscribers)
                            .Where(o => o.Owner.UserId != currentUser.UserId)
                            .ToListAsync());
            FilterOffers();
        }


        private void FilterOffers() => Offers = new ObservableCollection<Offer>( Offers.Where(o => FilterBySubscribed == o.Subscribers.Contains(new OfferSubscriber(o.OfferId, currentUser.UserId), new OfferSubscriberEqualityComparer())).ToList());

        public async Task Search(object value, ESearchCategory searchCategory)
        {
            Offers = new ObservableCollection<Offer>( await SearchCategoryToResults[searchCategory](value, Initializer.GetDefaultContext()));
            FilterOffers();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
