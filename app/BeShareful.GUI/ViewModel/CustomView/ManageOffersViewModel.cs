﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.Storage.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Initializer = BeShareful.Storage.Initializer.BeSharefulDatabaseInitializer;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.ObjectModel;
using BeShareful.GUI.Command;
using BeShareful.GUI.Result;

namespace BeShareful.GUI.ViewModel.CustomView
{
    public class ManageOffersViewModel : INotifyPropertyChanged
    {
        public int Quantity { get; set; }
        public string Location { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
        public EProductState ProductState { get; set; }
        public string ProductDescription { get; set; }
        public string Manufacturer { get; set; }
        public ECategoryType ProductCategory { get; set; }

        private ObservableCollection<Offer> _ownedOffers;
        public ObservableCollection<Offer> OwnedOffers
        {
            get => _ownedOffers;
            set
            {
                _ownedOffers = value;
                OnPropertyChanged(nameof(OwnedOffers));
            }
        }

        private User currentUser;

        private readonly BeSharefulContext databaseContext;

        private readonly IDictionary<ECommandType, Func<Offer, Task<ExecutionResult>>> CommandTypeToAsyncDelegate = new Dictionary<ECommandType, Func<Offer, Task<ExecutionResult>>>();
        private readonly IDictionary<ECommandType, Func<Offer, ExecutionResult>> CommandTypeToDelegate = new Dictionary<ECommandType, Func<Offer, ExecutionResult>>();

        private readonly IReadOnlyDictionary<ECommandType, ECommandType> CommandToInverted = new Dictionary<ECommandType, ECommandType>()
        {
            {ECommandType.Add, ECommandType.Delete },
            {ECommandType.Delete, ECommandType.Add }
        };

        private bool AreAllFieldsCompleted(Offer offer)
        {
            return !string.IsNullOrEmpty(offer.OfferedProduct.Name)
                && !string.IsNullOrEmpty(offer.Location)
                && offer.Quantity != 0;
        }

        private void PopulateCommandsDictionary()
        {
            CommandTypeToAsyncDelegate.Add(ECommandType.Delete, async (offer) =>
            {
                OwnedOffers.Remove(offer);
                databaseContext.Offers.Remove(offer);

                try
                {
                    await databaseContext.SaveChangesAsync();
                    return new ExecutionResult(true, "Offer removed successfully.");
                }
                catch (DbUpdateException e)
                {
                    return new ExecutionResult(false, e.InnerException.Message.Trim());
                }
            });

            CommandTypeToAsyncDelegate.Add(ECommandType.Add, async (offer) =>
            {
                if(!AreAllFieldsCompleted(offer))
                {
                    return new ExecutionResult(false, "Please complete all the required fields.");
                }

                OwnedOffers.Add(offer);
                databaseContext.Offers.Add(offer);

                try
                {
                    await databaseContext.SaveChangesAsync();
                    return new ExecutionResult(true, "Offer added successfully.");
                }
                catch (DbUpdateException e)
                {
                    return new ExecutionResult(false, e.InnerException.Message.Trim());
                }
            });

            CommandTypeToDelegate.Add(ECommandType.Add, (offer) =>
            {
                OwnedOffers.Add(offer);
                databaseContext.Offers.Add(offer);

                try
                {
                    databaseContext.SaveChanges();
                    return new ExecutionResult(true, "Offer added successfully.");
                }
                catch (DbUpdateException e)
                {
                    return new ExecutionResult(false, e.InnerException.Message.Trim());
                }
            });

            CommandTypeToDelegate.Add(ECommandType.Delete, (offer) =>
            {
                OwnedOffers.Remove(offer);
                databaseContext.Offers.Remove(offer);

                try
                {
                    databaseContext.SaveChanges();
                    return new ExecutionResult(true, "Offer removed successfully.");
                }
                catch (DbUpdateException e)
                {
                    return new ExecutionResult(false, e.InnerException.Message.Trim());
                }
            });

        }

        public async Task<ExecutionResult> ExecuteAsync(ECommandType commandType, Offer offer)
        {
            if(CommandTypeToAsyncDelegate.ContainsKey(commandType))
            {
                return await CommandTypeToAsyncDelegate[commandType](offer);
            }
            else
            {
                throw new NotSupportedException($"The command {commandType} is not yet supported.");
            }
        }

        public ExecutionResult Execute(ECommandType commandType, Offer offer)
        {
            if (CommandTypeToAsyncDelegate.ContainsKey(commandType))
            {
                return CommandTypeToDelegate[commandType](offer);
            }
            else
            {
                throw new NotSupportedException($"The command {commandType} is not yet supported.");
            }
        }

        public ExecutionResult Undo(ECommandType commandType, Offer offer)
        {
            if (CommandToInverted.ContainsKey(commandType))
            {
                return Execute(CommandToInverted[commandType], offer);
            }
            else
            {
                throw new NotSupportedException($"The command {commandType} doesn't support undo.");
            }
        }

        public async Task<ExecutionResult> UndoAsync(ECommandType commandType, Offer offer)
        {
            if(CommandToInverted.ContainsKey(commandType))
            {
                return await ExecuteAsync(CommandToInverted[commandType], offer);
            }
            else
            {
                throw new NotSupportedException($"The command {commandType} doesn't support undo.");
            }
        }

        public ManageOffersViewModel(User user)
        {
            currentUser = user;
            databaseContext = Initializer.GetDefaultContext();
            OwnedOffers = new ObservableCollection<Offer>();

            PopulateCommandsDictionary();
        }

        public async Task LoadAllOFfersAsync()
        {
            OwnedOffers = new ObservableCollection<Offer>(await databaseContext.Offers.Where(o => o.OwnerId == currentUser.UserId).Include(o => o.OfferedProduct).ToListAsync());
        }

        public async Task<Offer> GetCurrentOffer()
        {
            var product = new Product(ProductName, Price, ProductState, ProductDescription, Manufacturer, ProductCategory);
            var admin = await databaseContext.Users
                .Where(u => u.Type == EUserType.Admin)
                .OrderBy(u => u.MediatedOffers.Count)
                .FirstOrDefaultAsync();

            return new Offer
            {
                OfferedProduct = product,
                Quantity = Quantity,
                Location = Location,
                OwnerId = currentUser.UserId,
                MediatorId = admin.UserId
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
