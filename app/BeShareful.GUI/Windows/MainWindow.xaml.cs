﻿
using BeShareful.GUI.Configuration;
using BeShareful.GUI.Timing;
using BeShareful.GUI.Utility;
using BeShareful.GUI.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.ComponentModel;
using System.Timers;
using System.Windows;
using System.Windows.Media;

namespace BeShareful.GUI.Windows
{
    public partial class MainWindow : INotifyPropertyChanged
    {
        private TimedMessage _message;
        public TimedMessage Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
            Message = new TimedMessage();
        }
        
        private async void LogInButton_Clicked(object sender, RoutedEventArgs e)
        {
            if(DataContext is MainWindowViewModel viewModel)
            {
                var (success, message, user) = await viewModel.LoginAsync(PasswordBox.Password);
                Message.Value = message;
                GraphicsLogger.Log(Utilities.Logging.ELevel.Info);
                if (success && user != null)
                {
                    if(user.Type == Entities.Entity.Enum.EUserType.Regular)
                    {
                        var nextWindow = new UserDashboardWindow();
                        await nextWindow.InitializeAsync(user);
                        nextWindow.ShowDialog();
                    }
                    else
                    {
                        var nextWindow = new AdminDashboardWindow();
                        //await nextWindow.InitializeAsync(user);
                        nextWindow.ShowDialog();
                    }
                }
                else
                {

                }
            }
        }

        private void RegisterButton_Clicked(object sender, RoutedEventArgs e)
        {
            var registerWindow = new RegisterWindow() { Parent = this };
            registerWindow.DataContext = new RegisterWindowViewModel();
            registerWindow.ShowDialog();
        }

        private void ThemeChangeButton_Clicked(object sender, RoutedEventArgs e)
        {
            if(GUIConfiguration.Instance.CurrentBaseTheme == Theme.Light)
            {
                GUIConfiguration.Instance.SetBaseTheme(Theme.Dark);
                GUIConfiguration.Instance.CurrentThemeBrush =  GUIConfiguration.Instance.DarkThemeTitleBarBrush;
            }
            else
            {
                GUIConfiguration.Instance.SetBaseTheme(Theme.Light);
                GUIConfiguration.Instance.CurrentThemeBrush = GUIConfiguration.Instance.LightThemeTitleBarBrush;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
