﻿using BeShareful.Entities.Entity;
using BeShareful.GUI.ViewModel.CustomView;
using BeShareful.GUI.Windows.CustomViews;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Initializer = BeShareful.Storage.Initializer.BeSharefulDatabaseInitializer;

namespace BeShareful.GUI.Windows
{
    public partial class UserDashboardWindow : INotifyPropertyChanged
    {
        private readonly IDictionary<Type, object> ViewModels = new Dictionary<Type, object>();

        private bool _isManageOffersEnabled;
        public bool IsManageOffersEnabled
        {
            get => _isManageOffersEnabled;
            set
            {
                _isManageOffersEnabled = value;
                OnPropertyChanged(nameof(IsManageOffersEnabled));
            }
        }

        private bool _isSearchOffersEnabled;
        public bool IsSearchOffersEnabled
        {
            get => _isSearchOffersEnabled;
            set
            {
                _isSearchOffersEnabled = value;
                OnPropertyChanged(nameof(IsSearchOffersEnabled));
            }
        }

        private bool _isSearchSubscribedOffersEnabled;
        public bool IsSearchSubscribedOffersEnabled
        {
            get => _isSearchSubscribedOffersEnabled;
            set
            {
                _isSearchSubscribedOffersEnabled = value;
                OnPropertyChanged(nameof(IsSearchSubscribedOffersEnabled));
            }
        }

        private User _currentUser;
        public User CurrentUser 
        {
          get => _currentUser; 
          set
            {
                if(value != null)
                {
                    _currentUser = value;
                    UserCompleteName = $"{CurrentUser?.FirstName} {CurrentUser?.LastName}";
                }
            }
        }

        private string _userCompleteName;
        public string UserCompleteName
        {
            get => _userCompleteName;
            private set
            {
                _userCompleteName = value;
                OnPropertyChanged(nameof(UserCompleteName));
            }
        }

        private static readonly int ExpectedViewModelsCount = 3;
        private bool wasInitialized = false;

        public UserDashboardWindow()
        {
            InitializeComponent();

            ManageOffersView.ParentWindow = this;
        }

        private async Task InitializeViewModelsAsync()
        {
            if (CurrentUser == null)
            {
                throw new InvalidOperationException("The current user can't be null.");
            }

            var manageOffersViewModel = new ManageOffersViewModel(CurrentUser);
            ViewModels.Add(typeof(ManageOffersViewModel), manageOffersViewModel);

            var databaseContext = Initializer.GetDefaultContext();

            var searchOffersViewModel = new SearchOffersViewModel(databaseContext, CurrentUser);
            ViewModels.Add(typeof(SearchOffersViewModel), searchOffersViewModel);

            var searchSubscribedOffersViewModel = new SearchSubscribedOffersViewModel(CurrentUser, databaseContext, true);
            ViewModels.Add(typeof(SearchSubscribedOffersViewModel), searchSubscribedOffersViewModel);


            await manageOffersViewModel.LoadAllOFfersAsync();
            await searchOffersViewModel.LoadAllOffersAsync();
            await searchSubscribedOffersViewModel.SearchOffersViewModel.LoadAllOffersAsync();
        }

        public async Task InitializeAsync(User user)
        {
            CurrentUser = user;
            await InitializeViewModelsAsync();
            WireUpInvokers();
            SetInitialView();

            wasInitialized = true;
        }

        private void WireUpInvokers()
        {
            if(ViewModels.Count == ExpectedViewModelsCount)
            {
                ManageOffersView.UiInvoker = new Command.UserDashboardUIInvoker(ViewModels[typeof(ManageOffersViewModel)] as ManageOffersViewModel);
            }
        }

        private void SetInitialView()
        {
            IsManageOffersEnabled = true;
            DataContext = ViewModels[typeof(ManageOffersViewModel)];
            IsSearchOffersEnabled = false;
            IsSearchSubscribedOffersEnabled = false;
        }

        private void ManageOffersButton_Clicked(object sender, RoutedEventArgs e)
        {
            Debug.Assert(wasInitialized, "Please call the InitializeAsync(User) method after creating this window.");

            IsManageOffersEnabled = true;
            DataContext = ViewModels[typeof(ManageOffersViewModel)];
            IsSearchOffersEnabled = false;
            IsSearchSubscribedOffersEnabled = false;
        }

        private async void SearchButton_Clicked(object sender, RoutedEventArgs e)
        {
            Debug.Assert(wasInitialized, "Please call the InitializeAsync(User) method after creating this window.");

            IsSearchOffersEnabled = true;
            await (ViewModels[typeof(SearchOffersViewModel)] as SearchOffersViewModel).LoadAllOffersAsync();
            DataContext = ViewModels[typeof(SearchOffersViewModel)];
            IsManageOffersEnabled = false;
            IsSearchSubscribedOffersEnabled = false;
        }

        private async void SearchSubscribedButton_Clicked(object sender, RoutedEventArgs e)
        {
            Debug.Assert(wasInitialized, "Please call the InitializeAsync(User) method after creating this window.");

            IsSearchSubscribedOffersEnabled = true;
            await (ViewModels[typeof(SearchSubscribedOffersViewModel)] as SearchSubscribedOffersViewModel).SearchOffersViewModel.LoadAllOffersAsync();
            DataContext = ViewModels[typeof(SearchSubscribedOffersViewModel)];
            IsManageOffersEnabled = false;
            IsSearchOffersEnabled = false;
        }

        private void SettingsButton_Clicked(object sender, RoutedEventArgs e)
        {
            Debug.Assert(wasInitialized, "Please call the InitializeAsync(User) method after creating this window.");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        
    }
}
