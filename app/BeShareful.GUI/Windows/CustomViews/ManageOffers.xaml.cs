﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.GUI.Command;
using BeShareful.GUI.Timing;
using BeShareful.GUI.ViewModel.CustomView;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BeShareful.GUI.Windows.CustomViews
{
    public partial class ManageOffers : UserControl, INotifyPropertyChanged
    {
        public UserDashboardUIInvoker UiInvoker { get; set; }

        public MetroWindow ParentWindow { get; set; }

        private TimedMessage _message;
        public TimedMessage Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        private static readonly int Timeout = 2500;

        public ManageOffers()
        {
            InitializeComponent();
            Loaded += ManageOffers_Loaded;
            Message = new TimedMessage(Timeout);
        }

        private void ManageOffers_Loaded(object sender, RoutedEventArgs e)
        {
            ProductStateComboBox.ItemsSource = Enum.GetValues(typeof(EProductState)).Cast<EProductState>();
            ProductCategoryComboBox.ItemsSource = Enum.GetValues(typeof(ECategoryType)).Cast<ECategoryType>();
        }

        private async void AddNewOffer_Clicked(object sender, RoutedEventArgs e)
        {
            if (DataContext is ManageOffersViewModel viewModel)
            {
                var executionResult = await UiInvoker?.ExecuteAsync(ECommandType.Add, await viewModel.GetCurrentOffer());
                Message.Value = executionResult.Message;
            }
        }

        private async void DeleteButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (OffersDataGrid.SelectedItem == null)
            {
                return;
            }

            var executionResult = await UiInvoker?.ExecuteAsync(ECommandType.Delete, OffersDataGrid.SelectedItem as Offer);
            Message.Value = executionResult.Message;
        }

        private async void Undo_Clicked(object sender, RoutedEventArgs e)
        {
            var executionResult = await UiInvoker?.Undo();
            Message.Value = executionResult.Message;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
