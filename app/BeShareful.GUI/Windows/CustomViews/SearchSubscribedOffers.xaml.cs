﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.GUI.Command;
using BeShareful.GUI.Search;
using BeShareful.GUI.Timing;
using BeShareful.GUI.ViewModel.CustomView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BeShareful.GUI.Windows.CustomViews
{
    public partial class SearchSubscribedOffers : UserControl, INotifyPropertyChanged
    {
        private TimedMessage _message;
        public TimedMessage Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public SearchSubscribedOffers()
        {
            InitializeComponent();
            Message = new TimedMessage(2500);
            Loaded += SearchOffers_Loaded;
        }

        private void SearchOffers_Loaded(object sender, RoutedEventArgs e)
        {
            SearchCategoryComboBox.ItemsSource = Enum.GetValues(typeof(ESearchCategory)).Cast<ESearchCategory>();
            SearchCategoryComboBox.SelectedItem = ESearchCategory.Manufacturer;

            ProductStateComboBox.ItemsSource = Enum.GetValues(typeof(EProductState)).Cast<EProductState>();
            OfferStatusComboBox.ItemsSource = Enum.GetValues(typeof(EOfferStatus)).Cast<EOfferStatus>();
        }

        private async void SearchedText_Changed(object sender, TextChangedEventArgs e)
        {
            if (SearchCategoryComboBox.SelectedItem != null)
            {
                if (DataContext is SearchSubscribedOffersViewModel viewModel)
                {
                    await viewModel.SearchOffersViewModel.Search(SearchTextBox.Text, (ESearchCategory)SearchCategoryComboBox.SelectedItem);
                }
            }
        }
        private async void ProductStateComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ProductStateComboBox.SelectedItem != null)
            {
                if (DataContext is SearchSubscribedOffersViewModel viewModel)
                {
                    await viewModel.SearchOffersViewModel.Search(ProductStateComboBox.SelectedItem, ESearchCategory.State);
                }
            }
        }

        private async void OfferStatusComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OfferStatusComboBox.SelectedItem != null)
            {
                if (DataContext is SearchSubscribedOffersViewModel viewModel)
                {
                    await viewModel.SearchOffersViewModel.Search(OfferStatusComboBox.SelectedItem, ESearchCategory.Status);
                }
            }
        }

        private async void UnsubscribeButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (OffersDataGrid.SelectedItem != null)
            {
                if (DataContext is SearchSubscribedOffersViewModel viewModel)
                {
                    var executionResult = await viewModel.SearchOffersViewModel.UnsubscribeAsync(OffersDataGrid.SelectedItem as Offer);
                    Message.Value = executionResult.Message;
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
