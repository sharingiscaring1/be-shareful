﻿using BeShareful.Entities.Entity.Enum;
using BeShareful.GUI.ViewModel;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using BeShareful.GUI.Timing;

namespace BeShareful.GUI.Windows
{
    public partial class RegisterWindow 
    {
        public new Window Parent { get; set; }

        public RegisterWindow()
        {
            InitializeComponent();
            Loaded += RegisterWindow_Loaded;
        }

        private void RegisterWindow_Loaded(object sender, RoutedEventArgs e)
        {
            GenderCombobox.ItemsSource = Enum.GetValues(typeof(EUserGender)).Cast<EUserGender>();
            TypeComboBox.ItemsSource = Enum.GetValues(typeof(EUserType)).Cast<EUserType>();
        }

        private async void NextButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (DataContext is RegisterWindowViewModel viewModel)
            {
                var (wasRegistered, user) = await viewModel.RegisterAsync(UserPasswordBox.Password);
                if (wasRegistered)
                {
                    if(user.Type == EUserType.Regular)
                    {
                        var metadataWindow = new UserMetadataWindow();
                        metadataWindow.MainWindow = Parent;
                        metadataWindow.DataContext = new UserMetadataViewModel(user);
                        metadataWindow.ShowDialog();
                    }

                    Close();
                    if(Parent != null && Parent is MainWindow mainWindow)
                    {
                        mainWindow.Message.Value = "The register was successful.";
                    }
                }
            }
        }
    }
}
