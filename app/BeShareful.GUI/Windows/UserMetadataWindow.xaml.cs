﻿using BeShareful.Entities.Entity.Enum.UserMetadata;
using BeShareful.GUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BeShareful.GUI.Windows
{
    public partial class UserMetadataWindow
    {
        public Window MainWindow { get; set; }

        public UserMetadataWindow()
        {
            InitializeComponent(); 
            Loaded += RegisterWindow_Loaded;
        }

        private void RegisterWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ProfessionalStatusComboBox.ItemsSource = Enum.GetValues(typeof(EProfessionalStatus)).Cast<EProfessionalStatus>();
            DomainOfWorkComboBox.ItemsSource = Enum.GetValues(typeof(EDomainOfWork)).Cast<EDomainOfWork>();
        }

        private async void ContinueButton_Clicked(object sender, RoutedEventArgs e)
        {
            if(DataContext is UserMetadataViewModel viewModel)
            {
                var (wasAdded, _) = await viewModel.AddMetadataAsync();
                Close();
                //if (wasAdded)
                //{
                //    //if (MainWindow != null && MainWindow.DataContext is MainWindowViewModel mainWindowViewModel)
                //    //{
                //    //    mainWindowViewModel.Message = "The register was successful.";
                //    //}
                //    Close();
                //}
            }
        }
    }
}
