﻿using System.Collections.Generic;

namespace BeShareful.Entities.Security
{
    public struct Hashing
    {
        public enum Type
        {
            SHA256,
            BCrypt
        }

        public static IReadOnlyDictionary<string, Type> AlgorithmNameToType { get; } =  new Dictionary<string, Type>()
        {
            {"BCryptHasher", Type.BCrypt },
            {"SHA256Hasher", Type.SHA256 }
        };
    }
}
