﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BeShareful.Entities.Entity
{
    public class Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }

        public string Content { get; set; }
        public DateTime DateSent { get; set; }

        public virtual User Receiver { get; set; }

        public Message(string content, DateTime dateSent,User receiver)
        {
            Content = content;
            DateSent = dateSent;
            Receiver = receiver;
        }

        public Message()
        {
        }
    }
}
