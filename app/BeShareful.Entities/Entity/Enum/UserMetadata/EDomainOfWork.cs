﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Enum.UserMetadata
{
	public enum EDomainOfWork
	{
		IT,
		Automotive,
		Sales,
		Management,
		Other,
		None
	}
}
