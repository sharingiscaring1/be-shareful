﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Enum.UserMetadata
{
	public enum EProfessionalStatus
	{
		Student,
		Unemployed,
		Employed,
		Retired,
		RecentlyUnemployed
	}
}
