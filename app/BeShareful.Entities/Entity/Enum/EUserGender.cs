namespace BeShareful.Entities.Entity.Enum
{
	public enum EUserGender
	{
		Male,
		Female,
		Undefined
	}
}
