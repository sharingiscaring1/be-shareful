namespace BeShareful.Entities.Entity.Enum
{
	public enum ECategoryType
	{
		Books,
		Films,
		Electronics,
		Home,
		Beauty,
		Toys,
		Shoes,
		Watches,
		Clothes,
		Sports,
		Food
	}
}
