
namespace BeShareful.Entities.Entity.Enum
{
	public enum EProductState
	{
		New,
		Used,
		Old,
		Damaged
	}
}
