namespace BeShareful.Entities.Entity.Enum
{
	public enum EOfferStatus
	{
		Available,
		Unavailable
	}
}