namespace BeShareful.Entities.Entity.Enum
{
	public enum ETradeStatus
	{
		Posted,
		Waiting,
		Approved,
		Denied
	}
}