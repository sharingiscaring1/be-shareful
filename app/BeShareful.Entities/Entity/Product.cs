﻿using BeShareful.Entities.Entity.Enum;
using BeShareful.Entities.Entity.Join;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BeShareful.Entities.Entity
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public EProductState State { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }

        public ECategoryType CategoryType { get; set; }

        public Product(string name, double price, EProductState state, string description, string manufacturer,ECategoryType categoryType)
        {
            Name = name;
            Price = price;
            State = state;
            Description = description;
            Manufacturer = manufacturer;
            CategoryType = categoryType;
        }

        public Product()
        { }
    }
}
