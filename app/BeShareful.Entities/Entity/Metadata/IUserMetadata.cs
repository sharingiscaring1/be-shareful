﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Metadata
{
    public interface IUserMetadata
    {
        double ComputeScore();

        double Score { get; set; }

        IDictionary<string, (object, Type)> ProvidedMetadatas { get; }
    }
}
