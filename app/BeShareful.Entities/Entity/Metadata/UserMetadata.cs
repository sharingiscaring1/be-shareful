﻿using BeShareful.Entities.Entity.Enum.UserMetadata;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BeShareful.Entities.Entity.Metadata
{
    public class UserMetadata
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserMetadataId { get; set; } 

        public EProfessionalStatus ProfessionalStatus { get; set; }

        public EDomainOfWork DomainOfWork { get; set; }

        public double NetIncome { get; set; }

        public double Score { get; set; }

        public virtual User User { get; set; }

        private readonly IDictionary<string, Action<object>> ProvidedMetdataTypeToAssignment = new Dictionary<string, Action<object>>(); 

        private void PopulateProvidedMetadataDictionary()
        {
            ProvidedMetdataTypeToAssignment.Add(nameof(ProfessionalStatus), (value) => ProfessionalStatus = (EProfessionalStatus)value);
            ProvidedMetdataTypeToAssignment.Add(nameof(DomainOfWork), (value) => DomainOfWork = (EDomainOfWork)value);
            ProvidedMetdataTypeToAssignment.Add(nameof(NetIncome), (value) => NetIncome = (double)value);
        }

        private void Reconstruct(IUserMetadata metadata)
        {
            Score = metadata.ComputeScore();
            foreach (var item in metadata.ProvidedMetadatas)
            {
                string propertyName = item.Key;
                var (value, type) = item.Value;
                ProvidedMetdataTypeToAssignment[propertyName](value);
            }
        }

        public UserMetadata(IUserMetadata metadata)
        {
            PopulateProvidedMetadataDictionary();
            Reconstruct(metadata);
        }

        public UserMetadata()
        { }
    }
}
