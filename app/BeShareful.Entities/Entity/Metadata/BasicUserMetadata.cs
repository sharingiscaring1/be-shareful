﻿using BeShareful.Entities.Entity.Enum.UserMetadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Metadata
{
    public class BasicUserMetadata : IUserMetadata
    {
        public double Score { get; set; }

        public IDictionary<string, ValueTuple<object, Type>> ProvidedMetadatas { get; } 

        private static readonly double DefaultScore = 1.0;

        public BasicUserMetadata()
        {
            ProvidedMetadatas = new Dictionary<string, ValueTuple<object, Type>>();
            Score = DefaultScore;
        }

        public double ComputeScore() => Score;
    }
}
