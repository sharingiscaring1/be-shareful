﻿using BeShareful.Entities.Entity.Enum.UserMetadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Metadata.Decorator.MetadataWeight
{
    public static class Weights
    {
        public static IReadOnlyDictionary<EProfessionalStatus, double> ProfessionalStatusWeights { get; }
        public static IReadOnlyDictionary<EDomainOfWork, double> DomainOfWorkWeights { get; }

        static Weights()
        {
            ProfessionalStatusWeights = new Dictionary<EProfessionalStatus, double>()
            {
                {EProfessionalStatus.Unemployed, 250},
                {EProfessionalStatus.Student, 200},
                {EProfessionalStatus.RecentlyUnemployed, 150},
                {EProfessionalStatus.Retired, 100},
                {EProfessionalStatus.Employed, 50},
            };

            DomainOfWorkWeights = new Dictionary<EDomainOfWork, double>()
            {
                {EDomainOfWork.None, 500},
                {EDomainOfWork.Management, 450},
                {EDomainOfWork.Automotive, 400},
                {EDomainOfWork.IT, 300},
                {EDomainOfWork.Other, 250},
                {EDomainOfWork.Sales, 150},
            };
        }
    }
}
