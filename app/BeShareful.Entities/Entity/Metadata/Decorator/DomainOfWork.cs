﻿using BeShareful.Entities.Entity.Enum.UserMetadata;
using BeShareful.Entities.Entity.Metadata.Decorator.MetadataWeight;

namespace BeShareful.Entities.Entity.Metadata.Decorator
{
    public class DomainOfWork : UserMetadataDecorator
    {
        private EDomainOfWork domainOfWork;

        public DomainOfWork(EDomainOfWork domainOfWork, IUserMetadata metadata)
            : base(metadata)
        {
            this.domainOfWork = domainOfWork;
            Metadata.ProvidedMetadatas.Add(nameof(DomainOfWork), (domainOfWork, typeof(EDomainOfWork)));
        }

        public override double ComputeScore() => Metadata.ComputeScore() + Weights.DomainOfWorkWeights[domainOfWork];
    }
}
