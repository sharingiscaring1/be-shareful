﻿namespace BeShareful.Entities.Entity.Metadata.Decorator
{
    public class NetIncome : UserMetadataDecorator
    {
        private double netIncome;

        public NetIncome(double netIncome, IUserMetadata metadata)
            : base(metadata)
        {
            this.netIncome = netIncome;
            Metadata.ProvidedMetadatas.Add(nameof(NetIncome), (netIncome, typeof(double)));
        }

        public override double ComputeScore() => Metadata.ComputeScore() - netIncome;
    }
}
