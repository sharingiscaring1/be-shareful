﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Metadata.Decorator
{
    public abstract class UserMetadataDecorator : IUserMetadata
    {
        protected IUserMetadata Metadata { get; set; }
        public double Score { get => Metadata.Score; set => Metadata.Score = value; }
        public IDictionary<string, ValueTuple<object, Type>> ProvidedMetadatas => Metadata.ProvidedMetadatas; 

        public UserMetadataDecorator(IUserMetadata metadata)
        {
            Metadata = metadata;
        }

        public abstract double ComputeScore();
    }
}
