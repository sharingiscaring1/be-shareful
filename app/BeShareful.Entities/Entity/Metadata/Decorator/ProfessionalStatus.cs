﻿using BeShareful.Entities.Entity.Enum.UserMetadata;
using BeShareful.Entities.Entity.Metadata.Decorator.MetadataWeight;

namespace BeShareful.Entities.Entity.Metadata.Decorator
{
    public class ProfessionalStatus : UserMetadataDecorator
    {
        private EProfessionalStatus professionalStatus;

        public ProfessionalStatus(EProfessionalStatus professionalStatus, IUserMetadata metadata) 
            : base(metadata)
        {
            this.professionalStatus = professionalStatus;
            Metadata.ProvidedMetadatas.Add(nameof(ProfessionalStatus), (professionalStatus, typeof(EProfessionalStatus)));
        }

        public override double ComputeScore() => Metadata.ComputeScore() + Weights.ProfessionalStatusWeights[professionalStatus];
    }
}
