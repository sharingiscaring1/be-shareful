﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Join
{
    public class OfferSubscriberEqualityComparer : IEqualityComparer<OfferSubscriber>
    {
        public bool Equals(OfferSubscriber x, OfferSubscriber y)
        {
            return x.OfferId == y.OfferId && x.SubscriberId == y.SubscriberId;
        }

        public int GetHashCode(OfferSubscriber obj)
        {
            int hashCode = 1441190614;
            hashCode = hashCode * -1521134295 + obj.OfferId.GetHashCode();
            hashCode = hashCode * -1521134295 + obj.SubscriberId.GetHashCode();
            return hashCode;
        }
    }
}
