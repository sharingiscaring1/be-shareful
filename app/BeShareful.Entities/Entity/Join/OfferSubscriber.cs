﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeShareful.Entities.Entity.Join
{
    public class OfferSubscriber
    {

        public int OfferId { get; set; }
        public Offer Offer { get; set; }

        public int SubscriberId { get; set; }
        public User Subscriber { get; set; }

        public OfferSubscriber(Offer offer,User subscriber)
        {
            Offer = offer;
            Subscriber = subscriber;
        }

        public OfferSubscriber(int offerId, int subscriberId)
        {
            OfferId = offerId;
            SubscriberId = subscriberId;
        }

        public OfferSubscriber()
        {}

        public override bool Equals(object obj)
        {
            return obj is OfferSubscriber subscriber &&
                   OfferId == subscriber.OfferId &&
                   SubscriberId == subscriber.SubscriberId;
        }

        public override int GetHashCode()
        {
            int hashCode = 1441190614;
            hashCode = hashCode * -1521134295 + OfferId.GetHashCode();
            hashCode = hashCode * -1521134295 + SubscriberId.GetHashCode();
            return hashCode;
        }
    }
}
