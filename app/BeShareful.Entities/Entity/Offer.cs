﻿using BeShareful.Entities.Entity.Enum;
using BeShareful.Entities.Entity.Join;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BeShareful.Entities.Entity
{
    public class Offer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OfferId { get; set; }
        public int Quantity { get; set; }
        public EOfferStatus Status { get; set; }
        public double TotalPrice { get; set; }
        public string Location { get; set; }
        public DateTime DateAdded { get; set; }

        public int? OfferedProductId { get; set; }

        [ForeignKey(nameof(OfferedProductId))]
        public virtual Product OfferedProduct { get; set; }

        public int? OwnerId { get; set; }

        [ForeignKey(nameof(OwnerId))]
        public virtual User Owner { get; set; }

        public int? MediatorId { get; set; }

        [ForeignKey(nameof(MediatorId))]
        public virtual User Mediator { get; set; }


        public virtual ICollection<OfferSubscriber> Subscribers { get; set; }

        public Offer(User user, Product product, int quantity, string location)
        {
            Quantity = quantity;
            Owner = user;
            OfferedProduct = product;
            Status = EOfferStatus.Available;
            TotalPrice = OfferedProduct.Price * Quantity;
            Location = location;
            DateAdded = DateTime.Now;
            Subscribers = new HashSet<OfferSubscriber>();
        }

        public Offer()
        { }
    }
}
