﻿using BeShareful.Entities.Entity.Enum;
using BeShareful.Entities.Entity.Metadata;
using BeShareful.Entities.Security;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BeShareful.Entities.Entity.Join;

namespace BeShareful.Entities.Entity
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public Hashing.Type HashingType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EUserGender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Location { get; set; }
        public DateTime? DateRegistered { get; set; }
        public double? Rating { get; set; } = 0;
        public EUserType Type { get; set; }

        public virtual ICollection<Offer> PostedOffers { get; set; }

        public virtual ICollection<OfferSubscriber> SubscribedOffers { get; set; }

        public virtual ICollection<Offer> MediatedOffers { get; set; }

        public virtual ICollection<Message> ReceivedMessages { get; set; }

        public int? UserMetadataId { get; set; }

        [ForeignKey(nameof(UserMetadataId))]
        public virtual UserMetadata Metadata { get; set; }

        internal User(string username, string passwordHash, Hashing.Type hashingType, string firstName, string lastName, EUserGender gender, string phoneNumber, string location, DateTime dateRegistered, EUserType type)
        {
            Username = username;
            PasswordHash = passwordHash;
            HashingType = hashingType;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            PhoneNumber = phoneNumber;
            Location = location;
            DateRegistered = dateRegistered;
            Type = type;
            PostedOffers = new HashSet<Offer>();
        }

        public User()
        { }
    }
}
