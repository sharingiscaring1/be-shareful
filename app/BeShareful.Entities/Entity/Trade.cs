﻿using BeShareful.Entities.Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BeShareful.Entities.Entity
{
    public class Trade
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TradeId { get; set; }
        public ETradeStatus Status { get; set; }
        public string Reason { get; set; }
        public double Rating { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual User AcceptedUser { get; set; }

        public Trade(ETradeStatus status)
        {
            Status = status;
            Rating = 0.0;
        }

        public Trade()
        { }
    }
}
