﻿using BeShareful.Entities.Entity;
using BeShareful.Entities.Entity.Enum;
using BeShareful.Entities.Security;
using BeShareful.Utilities;
using BeShareful.Utilities.Logging;
using BeShareful.Utilities.Security;
using System;
using System.Collections.Generic;

namespace BeShareful.Entities.Factory
{
    public class UserFactory
    {
        public IHashingStrategy HashingStrategy { get; }
        public UserFactory(IHashingStrategy hashingStrategy)
        {
            HashingStrategy = hashingStrategy;
        }

        public User Create(string username, string password, string firstName, string lastName, EUserGender gender, string phoneNumber, string location, EUserType type)
        {
            string hashingAlgorithmName = HashingStrategy.GetType().Name;

            if(!Hashing.AlgorithmNameToType.ContainsKey(hashingAlgorithmName))
            {
                string message = $"The {hashingAlgorithmName} was not found in the algorithms registry. Please register it in the file Hashing.cs.";

                Configuration.Instance.Log(ELevel.Error, ELoggingType.Console, message);
                throw new KeyNotFoundException(message);
            }

            Configuration.Instance.Log(ELevel.Info, ELoggingType.Console, $"The user with username [{username}] was just created.");
            return new User(username, HashingStrategy.GetHash(password), Hashing.AlgorithmNameToType[hashingAlgorithmName], firstName, lastName, gender, phoneNumber, location, DateTime.Now, type);
        }
    }
}
